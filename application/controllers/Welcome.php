<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {
	public function __construct(){
        parent::__construct();
        
        $this->load->model('eventos_model');
    }
	
	public function index()
	{
        $dados["eventos"] = $this->eventos_model->listAllEvents();
		//exit($this->db->last_query());
		//print_r($dados["eventos"]);die();
		$this->load->view('header');
		$this->load->view('menu');
		$this->load->view('welcome_message', $dados);
		$this->load->view('footer');
	}
}
