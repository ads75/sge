<?php

class Eventos_model extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->table = "tb_eventos";
    }

    //Lista todos
    function listAllEvents()
    {
        //Monta a consulta
        $this->db->select("id, descricao, local, data_hora");
        $this->db->from($this->table);
        $this->db->order_by("id", "DESC");
        //Faz a consulta
        $query = $this->db->get();
        //Retorna todos os registros
        return $query->result();
    }
}