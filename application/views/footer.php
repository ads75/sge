<footer class="text-muted py-5">
  <div class="container">
    <p class="float-end mb-1">
      <a href="#">ir ao topo</a>
    </p>
    <p class="mb-1">e-Baladas &copy; <?=date('Y')?></p>
    <p class="mb-0">Aqui você encontra nosso documento dos <a href="/">termos e condições gerais</a> e nossa <a href="/docs/5.2/getting-started/introduction/">política de privacidade</a>.</p>
  </div>
</footer>

<script src="./assets/js/bootstrap.bundle.min.js" integrity="sha384-u1OknCvxWvY5kfmNBILK2hRnQC3Pr17a+RTT6rIHI7NnikvbZlHgTPOOmMi466C8" crossorigin="anonymous"></script>

      
  </body>
</html>



