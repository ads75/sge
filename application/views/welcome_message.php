<main>

  <section class="py-5 text-center container">
    <div class="row py-lg-5">
      <div class="col-lg-6 col-md-8 mx-auto">
        <h1 class="fw-light">e-Baladas</h1>
        <p class="lead text-muted">Encontre ingressos para grandes shows, teatro, musicais, festivais e eventos diversos no site da e-Baladas e fique por dentro das novidades!</p>
        <p>
          <a href="#" class="btn btn-primary my-2">Cadastre-se</a>
          <a href="#" class="btn btn-secondary my-2">Sua lista de compras</a>
        </p>
      </div>
    </div>
  </section>

  <div class="album py-5 bg-light">
    <div class="container">

      <div class="row row-cols-1 row-cols-sm-2 row-cols-md-3 g-3">
      <?php foreach($eventos as $rows): ?>  
      <div class="col">
          <div class="card shadow-sm">
            <svg class="bd-placeholder-img card-img-top" width="100%" height="225" xmlns="http://www.w3.org/2000/svg" role="img" aria-label="Placeholder: Thumbnail" preserveAspectRatio="xMidYMid slice" focusable="false"><title>Placeholder</title><rect width="100%" height="100%" fill="#55595c"/><text x="50%" y="50%" fill="#eceeef" dy=".3em">Thumbnail</text></svg>

            <div class="card-body">
              <p class="card-text"><?php echo $rows->descricao; ?></p>
              <div class="d-flex justify-content-between align-items-center">
                <div class="btn-group">
                  <button type="button" class="btn btn-sm btn-outline-secondary">Detalhes</button>
                  <button type="button" class="btn btn-sm btn-outline-secondary">Compartilhar</button>
                </div>
                <small class="text-muted"><?php echo $rows->data_hora; ?></small>
              </div>
            </div>
          </div>
        </div>

      <?php endforeach; ?>
      </div>
    </div>
  </div>

</main>

