<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
class Funcoes_diversas
{
    //$CI =& get_instance();
    public static function curl($dados){

        $ch = curl_init("http://".$dados->ip."/".$dados->porta);

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($ch);

        @curl_close($ch);

        return $response;

    }    

    public static function formataMoeda($valor) {
        $verificaPonto = ".";
        if(strpos("[".$valor."]", "$verificaPonto")):
            $valor = str_replace('.','', $valor);
            $valor = str_replace(',','.', $valor);
        else:
            $valor = str_replace(',','.', $valor);
        endif;

        return $valor;
    }

    public static function formataData($data)
    {
        $date = date_create();
        date_timestamp_set($date, $data);
        return date_format($date, 'd/m/Y H:i');
    }

    public static function formataDataHora($data)
    {
        $date = date_create();
        date_timestamp_set($date, $data);
        return date_format($date, 'Y-m-d H:i');
    }

    //usando para grid calcular a hora excedente do periodo
    public static function formataDataHoraComSegundos($data)
    {
        $date = date_create();
        date_timestamp_set($date, $data);
        return date_format($date, 'Y-m-d H:i:s');
    }

    public static function formataDataHoraIni($data)
    {
        $date = date_create();
        date_timestamp_set($date, $data);
        $data_hora_ini = date_format($date, 'd/m/Y H:i');
        $array = explode(" ", $data_hora_ini);
        $data = $array[0];
        $hora = $array[1];
        return $data."T".$hora;
    }

    public static function formataDataParaCalcularMulta($data)
    {
        //, strtotime('+3 Hours')
        $date = date_create();
        date_timestamp_set($date, $data);
        return date_format($date, 'Y/m/d H:i:s');
    }

    public static function formatDate($data)
    {
        $date = date_create();
        date_timestamp_set($date, $data);
        return date_format($date, 'd/m/Y');
    }

    //valida se uma data rm BR é verdadeira
    public static function validaData($data){
        $data = explode("/","$data"); // fatia a string $dat em pedados, usando / como referência
        $d = $data[0];
        $m = $data[1];
        $y = $data[2];

        // verifica se a data é válida!
        // 1 = true (válida)
        // 0 = false (inválida)
        $res = checkdate($m,$d,$y);
        if ($res == 1){
            return true;
        } else {
            return false;
        }
    }

    //converte data BR para UNIX
    public static function convertDataToUnix($data)
    {
        $data = strtotime(str_replace('/', '-', $data));
        return $data;
    }

    public static function checkPermissionAction($action)
    {
        $CI =& get_instance();
        if (in_array($action, $CI->session->userdata("controller"))):
            return true;
        else:
            return false;
        endif;
    }

    public static function acessoNegado()
    {
        $CI =& get_instance();

        $CI->load->view("admin/header");
        $CI->load->view("admin/top_bar");
        $CI->load->view("admin/menu");
        $CI->load->view("errors/html/error_denied");
        $CI->load->view("admin/footer");
    }

    public static function encrypt_decrypt($action, $string)
    {
        $output = false;
        $encrypt_method = "AES-256-CBC";
        $secret_key = 'a6a94e132de13792ec3f1477dcbeb296574cea25'; //sha1
        $secret_iv = '1fdd8a43a4fdc777c9b153d2e6561755'; //md5
        // hash
        $key = hash('sha256', $secret_key);

        // iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning
        $iv = substr(hash('sha256', $secret_iv), 0, 16);
        if ($action == 'encrypt') {
            $output = openssl_encrypt($string, $encrypt_method, $key, 0, $iv);
            $output = base64_encode($output);
        } else if ($action == 'decrypt') {
            $output = openssl_decrypt(base64_decode($string), $encrypt_method, $key, 0, $iv);
        }
        return $output;
    }

    //remover pontos e traço antes de inserir usuário
    public static function removerMascaraCpf($cpf){

        // Elimina possivel mascara
        $cpf = preg_replace("/[^0-9]/", "", $cpf);
        $cpf = str_pad($cpf, 11, '0', STR_PAD_LEFT);

        return $cpf;
    }

    //remover pontos e traço antes de inserir usuário
    public static function removerMascaraTelefone($telefone){
        // Elimina possivel mascara
        $telefone = trim($telefone);
        $telefone = str_replace("(", "", $telefone);
        $telefone = str_replace(")", "", $telefone);
        $telefone = str_replace(" ", "", $telefone);
        $telefone = str_replace("-", "", $telefone);
        return $telefone;
    }

    public static function removerCaracteresEspeciais($string){
            // matriz de entrada
            //$what = array( 'ä','ã','à','á','â','ê','ë','è','é','ï','ì','í','ö','õ','ò','ó','ô','ü','ù','ú','û','À','Á','É','Í','Ó','Ú','ñ','Ñ','ç','Ç',' ','-','(',')',',',';',':','|','!','"','#','$','%','&','/','=','?','~','^','>','<','ª','º' );

            // matriz de saída
            //$by   = array( 'a','a','a','a','a','e','e','e','e','i','i','i','o','o','o','o','o','u','u','u','u','A','A','E','I','O','U','n','n','c','C','_','_','_','_','_','_','_','_','_','_','_','_','_','_','_','_','_','_','_','_','_','_','_' );

            // devolver a string
            //return str_replace($what, $by, $string);
        return preg_replace(array("/(á|à|ã|â|ä)/","/(Á|À|Ã|Â|Ä)/","/(é|è|ê|ë)/","/(É|È|Ê|Ë)/","/(í|ì|î|ï)/","/(Í|Ì|Î|Ï)/","/(ó|ò|õ|ô|ö)/","/(Ó|Ò|Õ|Ô|Ö)/","/(ú|ù|û|ü)/","/(Ú|Ù|Û|Ü)/","/(ç|ḉ)/","/(Ç|Ḉ)/","/(ñ)/","/(Ñ)/"),explode(" ","a A e E i I o O u U c C n N"),$string);
    }

    //apagar arquivos de um diretorio
    public static function deletarArquivosDiretorio($patch = null)
    {
        $files = glob($patch.'*'); // get all file names
        foreach($files as $file){ // iterate files
            if(is_file($file))
                unlink($file); // delete file
        }
    }

    //formatar datatime-local para epoch
    public static function formataDataTimeLocalToEpoch($data = null){
        $array = explode("T", $data);
        $data = $array[0] . " " . $array[1];
        $data = strtotime($data);
        return $data;
    }

    //formatar datatime-local para epoch
    public static function formataDataTimeLocalToBr($data = null){
        $array = explode("T", $data);
        $data = implode("/", array_reverse(explode("-", $array[0]))) . " " . $array[1];
        return $data;
    }

    //calcular tempo de permanência
    public static function calcularTempo($inicio = null, $fim = null, $status = null, $quarto = null){
        //$CI =& get_instance();
        $data_inicial = $inicio;

        if($fim == "") {
            $data_final = date("d") . "/" . date("m") . "/" . date("Y") . "T" . date("H:i");
        }else{
            $data_final =  $fim;
        }

        $arrayDtInicio = explode("T", $data_inicial);
        $data_hora_inicio_flag = implode("-", array_reverse(explode("/", $arrayDtInicio[0])));
        $data_hora_inicio = $data_hora_inicio_flag . " " . $arrayDtInicio[1];
        $data_hora_inicio = str_replace("T", " ", $data_hora_inicio);

        $arrayDtFim = explode("T", $data_final);
        $data_hora_fim_flag = implode("-", array_reverse(explode("/", $arrayDtFim[0])));
        $data_hora_fim = $data_hora_fim_flag . " " . $arrayDtFim[1];
        $data_hora_fim = str_replace("T", " ", $data_hora_fim);

        $start_date = new DateTime($data_hora_inicio);
        $since_start = $start_date->diff(new DateTime($data_hora_fim));
        $total = $since_start->days;
        $anos = $since_start->y;
        $meses = $since_start->m;
        $dias = $since_start->d;
        $horas = $since_start->h;
        $minutos = $since_start->i;
        $segundos = $since_start->s;


        $html = $dias." dias ".$horas."h ".$minutos."m";

        return $html;
    }

    public static function formataMes($mes){
        switch($mes){
            case 1:
                $mes = "Janeiro";
                break;
            case 2:
                $mes = "Fevereiro";
                break;
            case 3:
                $mes = "Março";
                break;
            case 4:
                $mes = "Abril";
                break;
            case 5:
                $mes = "Maio";
                break;
            case 6:
                $mes = "Junho";
                break;
            case 7:
                $mes = "Julho";
                break;
            case 8:
                $mes = "Agosto";
                break;
            case 9:
                $mes = "Setembro";
                break;
            case 10:
                $mes = "Outubro";
                break;
            case 11:
                $mes = "Novembro";
                break;
            case 12:
                $mes = "Dezembro";
                break;
        }
        return $mes;
    }

    //transforma hora inteiro em minutos
    public static function horaParaMinutos($hora){
        $partes = explode(":", $hora);
        $minutos = $partes[0]*60+$partes[1];
        return ($minutos);
   }

    public static function getPing($host, $port, $timeout){       
        $tB = microtime(true); 
        $fP = @fSockOpen($host, $port, $errno, $errstr, $timeout); 
        if (!$fP) { return "down"; } 
        $tA = microtime(true); 
        return round((($tA - $tB) * 1000), 0)." ms";
    }

    public static function get_inicio_fim_semana($numero_semana = "", $ano = "")
    {
        /* soma o número de semanas em cima do início do ano 01/01/2013 */
        $semana_atual = strtotime('+'.$numero_semana.' weeks', strtotime($ano.'0101'));

    /*
    pega o número do dia da semana
    0 - Domingo
    ...
    6 - Sábado
    */
    $dia_semana = date('w', $semana_atual);

        /*
        diminui o dia da semana sobre o dia da semana atual
        ex.: $semana_atual: 10/09/2013 terça-feira
        $dia_semana: 2 (terça-feira)
        $data_inicio_semana: 08/09/2013
        */
        $data_inicio_semana = strtotime('-'.$dia_semana.' days', $semana_atual);

        /* Data início semana */
        $primeiro_dia_semana = date('Y-m-d', $data_inicio_semana);

        /* Soma 6 dias */
        $ultimo_dia_semana = date('Y-m-d', strtotime('+6 days', strtotime($primeiro_dia_semana)));

        /* retorna */
        return array($primeiro_dia_semana, $ultimo_dia_semana);
    }

    public static function calcularQtdHorasPermanencia($inicio = null)
    {
        $datatime1 = new DateTime($inicio);
        $dataAtual = date("Y/m/d H:i:s");

        $datatime2 = new DateTime($dataAtual);

        $data1 = $datatime1->format('Y-m-d H:i:s');
        $data2 = $datatime2->format('Y-m-d H:i:s');

        $diff = $datatime1->diff($datatime2);
        $horas = $diff->h + ($diff->days * 24);
        return $horas;
        //return $this->insertInPosition($horas, 1, '.');
    }
}
